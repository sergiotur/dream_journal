import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TextEditorComponent} from "./pages/main/new-dream/text-editor/text-editor.component";
import {DreamDetailsComponent} from "./pages/main/home/board/dream-details/dream-details.component";

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'main',
        loadChildren: () => import('./pages/main/main.module').then(m => m.MainModule)
    },
    {path: 'text-editor', component: TextEditorComponent},
    {path: 'details/:id', component: DreamDetailsComponent},
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
