import {Moment} from 'moment';

export interface IDream {
    author: IAuthor;
    createdAt: string | Moment;
    updatedAt: string | Moment;
    photo: string;
    title: string;
    description: string;
    likesCount: number;
    commentsCount: number;
    sharedCount: number;
    favorite: boolean;
}

interface IAuthor {
    avatar: string;
    name: string;
    isConfirmed: boolean;
}
