import {IDream} from '../interfaces/dream.interface';
import * as moment from 'moment';

export const DREAMS_MOCK: IDream[] = [
    {
        author: {
            name: 'Randall Steward',
            avatar: 'assets/images/man.jpg',
            isConfirmed: true
        },
        photo: 'assets/images/winter.jpg',
        title: 'Become the king of Winterfell',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget quis suspendisse ut felis vitae cras cursus arcu diam. Consequat faucibus faucibus at faucibus.',
        createdAt: moment('2020-06-25').fromNow(),
        updatedAt: moment().subtract(2, 'hours').fromNow(),
        likesCount: 124,
        commentsCount: 95,
        sharedCount: 12,
        favorite: false
    },
    {
        author: {
            name: 'Samantha William',
            avatar: 'assets/images/woman-1.jpeg',
            isConfirmed: false
        },
        photo: 'assets/images/sea.jpg',
        title: 'The Beach and My Lover',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget quis suspendisse ut felis vitae cras cursus arcu diam. Consequat faucibus faucibus at faucibus.',
        createdAt: moment('2020-06-23').fromNow(),
        updatedAt: moment().subtract(2, 'hours').fromNow(),
        likesCount: 124,
        commentsCount: 95,
        sharedCount: 12,
        favorite: false
    },
    {
        author: {
            name: 'Sylvia Aurora',
            avatar: 'assets/images/woman-2.png',
            isConfirmed: false
        },
        photo: 'assets/images/tiger.jpg',
        title: 'Become a Queen in Indian Jungle',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget quis suspendisse ut felis vitae cras cursus arcu diam. Consequat faucibus faucibus at faucibus.',
        createdAt: moment('2020-06-21').fromNow(),
        updatedAt: moment().subtract(2, 'hours').fromNow(),
        likesCount: 124,
        commentsCount: 95,
        sharedCount: 12,
        favorite: false
    }
];
