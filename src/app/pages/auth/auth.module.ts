import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {AuthRoutingModule} from './auth.routing';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {SignUpComponent} from "./sign-up/sign-up.component";

@NgModule({
    declarations: [AuthComponent, SignInComponent, SignUpComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        IonicModule,
        ReactiveFormsModule
    ],
    exports: [
        AuthRoutingModule
    ]
})
export class AuthModule {
}
