import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss', '../auth.component.scss']
})
export class SignInComponent implements OnInit {

    public loginForm: FormGroup;

    public inputType = 'password';

    constructor(private formBuilder: FormBuilder, public router: Router) {
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(4)]]
        });
    }

    signIn() {
        this.router.navigateByUrl('/main/home');
    }

    onSwitchInputType() {
        this.inputType = this.inputType === 'password' ? 'text' : 'password';
    }

}
