import { Component, OnInit } from '@angular/core';
import {ActionSheetController, AlertController} from "@ionic/angular";
import {Router} from "@angular/router";

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
})
export class TextEditorComponent implements OnInit {

  constructor(public router: Router, private alertCtrl: AlertController, private actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {}

  async onSaveDraft() {
    const alert = await this.alertCtrl.create({
      cssClass: 'editor-alert',
      header: 'Save draft?',
      message: 'Save to your draft list and you can come back later to finish this post.',
      buttons: [
        {
          text: 'discard',
          cssClass: 'cancel-text',
          handler: (blah) => {
            this.router.navigateByUrl('/main/new-dream');
          }
        }, {
          text: 'save draft',
          cssClass: 'confirm-text',
          handler: () => {
            this.router.navigateByUrl('/main/new-dream');
          }
        }
      ]
    });
    await alert.present();
  }

  async onSave() {
    const sheet = await this.actionSheetCtrl.create({
      cssClass: 'my-custom-class',
      mode: 'ios',
      buttons: [ {
        text: 'Lorem Ipsum',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Lorem Ipsum',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Lorem Ipsum',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Save',
        role: 'cancel',
        handler: () => {
          this.router.navigateByUrl('/main/new-dream');
        }
      }]
    });
    await sheet.present();
  }
}
