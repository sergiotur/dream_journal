import { Component, OnInit } from '@angular/core';
import { Plugins, CameraResultType } from '@capacitor/core';
import {Router} from "@angular/router";
import {Location} from "@angular/common";
const { Camera } = Plugins;


@Component({
  selector: 'app-new-dream',
  templateUrl: './new-dream.component.html',
  styleUrls: ['./new-dream.component.scss'],
})
export class NewDreamComponent implements OnInit {

  public image;

  constructor(private router: Router, public location: Location) { }

  ngOnInit() {}

  async getPhoto(){
    let image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      correctOrientation: false,
      resultType: CameraResultType.Uri
    });
    this.image = image.webPath;
  }

  goToHome() {
      this.router.navigateByUrl('/main/home/board')
  }

  toEditor() {
    this.router.navigateByUrl('/text-editor');
  }

}
