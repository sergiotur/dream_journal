import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainRoutingModule} from './main.routing';
import {MainComponent} from './main.component';
import {IonicModule} from '@ionic/angular';
import {NotificationsComponent} from "./notifications/notifications.component";
import {FavoritesComponent} from "./favorites/favorites.component";
import {NewDreamComponent} from "./new-dream/new-dream.component";
import {FormsModule} from "@angular/forms";
import {TextEditorComponent} from "./new-dream/text-editor/text-editor.component";

@NgModule({
    declarations: [MainComponent, NotificationsComponent, FavoritesComponent, NewDreamComponent, TextEditorComponent],
    imports: [
        CommonModule,
        MainRoutingModule,
        IonicModule,
        FormsModule
    ],
    exports: [
        MainRoutingModule
    ]
})
export class MainModule {
}
