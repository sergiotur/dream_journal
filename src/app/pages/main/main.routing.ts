import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {FavoritesComponent} from './favorites/favorites.component';
import {NewDreamComponent} from "./new-dream/new-dream.component";
import {TextEditorComponent} from "./new-dream/text-editor/text-editor.component";

const routes: Routes = [
    {
        path: '', component: MainComponent, children: [
            {path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)},
            {path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule)},
            {path: 'notifications', component: NotificationsComponent},
            {path: 'favorites', component: FavoritesComponent},
            {path: 'new-dream', component: NewDreamComponent},
            {path: 'edit-dream/:id', component: NewDreamComponent},
            {path: '', redirectTo: 'home', pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule {
}
