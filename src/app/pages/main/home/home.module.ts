import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home.routing';
import {BoardComponent} from './board/board.component';
import {IonicModule} from '@ionic/angular';
import {DreamsListComponent} from './board/dreams-list/dreams-list.component';
import {DreamComponent} from './board/dreams-list/dream/dream.component';
import {DreamDetailsComponent} from "./board/dream-details/dream-details.component";

@NgModule({
    declarations: [HomeComponent, BoardComponent, DreamsListComponent, DreamComponent, DreamDetailsComponent],
    imports: [
        CommonModule,
        HomeRoutingModule,
        IonicModule
    ],
    exports: [
        HomeRoutingModule
    ]
})
export class HomeModule {
}
