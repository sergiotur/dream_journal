import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";

@Component({
  selector: 'app-dream-details',
  templateUrl: './dream-details.component.html',
  styleUrls: ['./dream-details.component.scss'],
})
export class DreamDetailsComponent implements OnInit {

  constructor(public location: Location) { }

  ngOnInit() {}

}
