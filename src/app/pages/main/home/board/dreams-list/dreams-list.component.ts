import {Component, Input, OnInit} from '@angular/core';
import {IDream} from '../../../../../shared/interfaces/dream.interface';

@Component({
    selector: 'app-dreams-list',
    templateUrl: './dreams-list.component.html',
    styleUrls: ['./dreams-list.component.scss'],
})
export class DreamsListComponent implements OnInit {

    @Input() dreams: IDream[] = [];

    constructor() {
    }

    ngOnInit() {
    }
}
