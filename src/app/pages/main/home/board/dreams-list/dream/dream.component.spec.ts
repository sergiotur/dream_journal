import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DreamComponent } from './dream.component';

describe('DreamComponent', () => {
  let component: DreamComponent;
  let fixture: ComponentFixture<DreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DreamComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
