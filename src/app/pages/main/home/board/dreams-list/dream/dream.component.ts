import {Component, Input, OnInit} from '@angular/core';
import {IDream} from '../../../../../../shared/interfaces/dream.interface';
import * as moment from 'moment';
import {Router} from "@angular/router";

@Component({
    selector: 'app-dream',
    templateUrl: './dream.component.html',
    styleUrls: ['./dream.component.scss'],
})
export class DreamComponent implements OnInit {

    @Input() dream: IDream = null;

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    toDetails() {
        this.router.navigateByUrl('/details/1')
    }

}
