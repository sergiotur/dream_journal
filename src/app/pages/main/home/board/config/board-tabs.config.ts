import {IBoardTab} from "../interfaces/board-tab.interface";

export const BOARD_TABS_CONFIG: IBoardTab[] = [
    {
        label: 'Newest'
    },
    {
        label: 'Popular'
    },
    {
        label: 'Following'
    },
    {
        label: 'Other'
    }
]