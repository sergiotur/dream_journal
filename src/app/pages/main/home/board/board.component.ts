import {Component, OnInit} from '@angular/core';
import {IDream} from '../../../../shared/interfaces/dream.interface';
import {DREAMS_MOCK} from '../../../../shared/mocks/dreams.mock';
import {IBoardTab} from "./interfaces/board-tab.interface";
import {BOARD_TABS_CONFIG} from "./config/board-tabs.config";

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {

    public dreams: IDream[] = DREAMS_MOCK;

    public tabsConfig: IBoardTab[] = BOARD_TABS_CONFIG;

    constructor() {
    }

    ngOnInit() {
    }

}
