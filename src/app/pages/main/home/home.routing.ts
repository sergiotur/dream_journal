import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {BoardComponent} from './board/board.component';
import {DreamDetailsComponent} from "./board/dream-details/dream-details.component";

const routes: Routes = [
    {
        path: '', component: HomeComponent, children: [
            {path: 'board', component: BoardComponent},
            {path: '', redirectTo: 'board', pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomeRoutingModule {
}
