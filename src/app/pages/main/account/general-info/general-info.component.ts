import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Component({
    selector: 'app-general-info',
    templateUrl: './general-info.component.html',
    styleUrls: ['./general-info.component.scss'],
})
export class GeneralInfoComponent implements OnInit {

    constructor(public location: Location, private router: Router) {
    }

    ngOnInit() {
    }

    public logOut() {
        localStorage.clear();
        this.router.navigateByUrl('/auth/sign-in');
    }

}
