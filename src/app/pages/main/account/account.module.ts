import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountRoutingModule} from './account.routing';
import {AccountComponent} from './account.component';
import {GeneralInfoComponent} from './general-info/general-info.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
    declarations: [AccountComponent, GeneralInfoComponent],
    imports: [
        CommonModule,
        AccountRoutingModule,
        IonicModule
    ],
    exports: [
        AccountRoutingModule
    ]
})
export class AccountModule {
}
