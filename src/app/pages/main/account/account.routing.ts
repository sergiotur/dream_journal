import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account.component';
import {GeneralInfoComponent} from './general-info/general-info.component';

const routes: Routes = [
    {
        path: '', component: AccountComponent, children: [
            {path: 'general-info', component: GeneralInfoComponent},
            {path: '', redirectTo: 'general-info', pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule {
}
